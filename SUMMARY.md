# Summary

* [Introduction](README.md)

## OFFICIAL DOC
* [Gitlab CI](https://gitlab.com/help/ci/quick_start/README.md)
* [.yml config](https://gitlab.com/help/ci/yaml/README.md)
* [runner](https://gitlab.com/help/ci/runners/README.md)
* [java example](https://about.gitlab.com/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/)
